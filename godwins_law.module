<?php
/**
 * @file godwins_law.module
 * The main module file defines Drupal hooks and primary functionality.
 */

define('GODWINS_LAW_NOACTION', -1);

/**
 * Implements hook_menu().
 */
function godwins_law_menu() {
  // Set a menu item for the admin configuration page for Godwin's Law.
  $items['admin/config/content/godwins_law'] = array(
    'title' => 'Godwin\'s Law',
    'description' => 'Administer how the site should behave when Godwin\'s Law has been invoked in a comment or discussion.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('godwins_law_admin_settings'),
    'access arguments' => array('administer godwins law'),
    'file' => 'godwins_law.admin.inc',
    'weight' => 5,
  );
  return $items;
}

/**
 * Implementation of hook_form_alter().
 */
function godwins_law_form_alter(&$form, &$form_state, $form_id) {
  // TODO: Limit this to specified node types, as well.
  if (strpos($form_id, 'comment_node_') === 0) {
    $form['#submit'][] = '_godwins_law_comment_node_submit';
  }
}

/**
 * A form submit handler that's been added to comment forms. This submit handler
 * looks for references to the keywords defined at admin/config/godwins_law and
 * will disable any further comments on the node if found.
 */
function _godwins_law_comment_node_submit($form, &$form_state) {
  $comment_body = $form_state['values']['comment_body'][$form_state['values']['language']][0]['value'];
  $keywords = variable_get('godwins_law_keywords', array('hitler', 'nazi'));

  // Create a regex pattern based on the keywords defined.
  $pattern  = '/' . implode('|', array_map('preg_quote', $keywords)) . '/i';

  // If any of the offending keywords are found in the comment body, set the
  // node's comments to be closed (COMMENT_NODE_CLOSED) and alert the user that
  // (s)he's ruined the fun for everyone.
  if(preg_match($pattern, $comment_body)) {
    $action = variable_get('godwins_law_action', COMMENT_NODE_CLOSED);
    if ($action != GODWINS_LAW_NOACTION) {
      $nid = $form_state['comment']->nid;
      $node = node_load($nid);
      $node->comment = ($node->comment) ? COMMENT_NODE_CLOSED : $node->comment;
      node_save($node);

      // Determine the message to display to the offending user.
      $message = variable_get('godwins_law_message', t('This comment has invoked Godwin\'s Law. Comments for %node_title are now closed.'));
      $message = format_string($message, array('%node_title' => $node->title));
      drupal_set_message($message);
    }
    else {
      drupal_set_message(variable_get('godwins_law_message_noaction', t('This comment has invoked Godwin\'s Law. No other action has been taken.')));
    }

    $watchdog_args = array(
      '%node_title'     => $node->title,
      '@nid'            => $node->nid,
      '@comment_status' => variable_get('godwins_law_action', COMMENT_NODE_CLOSED),
      '@uid'            => $form_state['comment']->uid,
    );
    watchdog('godwins_law', 'The node \'%node_title\' (NID: @nid) had comments set to @comment_status because a user (UID: @uid) invoked Godwin\'s Law.', $watchdog_args);
  }
}
